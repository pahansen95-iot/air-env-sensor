import alert
import edgeclient
import argparse
import os
import multiprocessing as mp
import logging as log
import sys
import time
import signal
import sensorhardware.bme680 as sensordevice

ERROR_UNEXPECTED_ALERT = 1
ERROR_MQTT_MISCONFIGURED = 2

backupFilePath = os.path.join(".", "backup.txt")

class SigTerm:
    def __init__(self, quitPipe):
        self.chan = quitPipe
        signal.signal(signal.SIGTERM, self.callback)

    def callback(self):
        print("SIGTERM has been recieved. Now Gracefully Terminating.")
        log.info("SIGTERM has been recieved. Now Gracefully Terminating.")
        self.chan.put("quit")

if __name__ == "__main__":
    #cmd line arguements
    ap = argparse.ArgumentParser()

    #define arguements
    ap.add_argument("--config", default=os.path.join(".", "conf.yaml"),help="Path of the config file used by the MQTT Client.")
    ap.add_argument("--log", "-l", default="stdout", help="Path to which the log is written. Defualts to stdout.")
    ap.add_argument("--poll", "-p", default=60.0,type=float,help="Time in seconds between sensor pollings.")
    
    #parse arguements
    args = ap.parse_args()

    #setup logging
    if "stdout" not in args.log:
        log.basicConfig(filename=args.log)

    #setup data stream
    dataQueue = mp.Queue()

    #init Sensor object
    sensor = sensordevice.Sensor(delta=args.poll, stream=dataQueue)

    #create edgeclient
    if os.path.exists(args.config): #check if config file exists
        ec = edgeclient.Publisher(config=edgeclient.load_config(args.config))
    else:
        log.warning("Config file at path \"{}\" does not exist. MQTT Client will not work. Now Exiting".format(args.config))
        print("Config file at path \"{}\" does not exist. MQTT Client will not work. Now Exiting.".format(args.config))
        exit(ERROR_MQTT_MISCONFIGURED)

    #get channel to listen for the mqtt client to quit
    quitChan = ec.get_quit_listener()

    #setup signal handling to stop the edgeclient
    SigTerm(ec.quit_pipe)

    #connect edgeclient
    ec.connect()

    #begin upload
    ec.publish(dataQueue)

    #begin collecting data
    sensor.start()

    #check if there is any backup data to upload
    if os.path.exists(backupFilePath):
        with open(backupFilePath,"r") as f:
            for line in f:
                try:
                    dataQueue.put(false)
                except:
                    pass
        #for now archive the backup
        os.rename(backupFilePath, "backup-{}.txt".format(time.asctime()))

    #block till "done" is sent by mqtt client
    while True:
        if quitChan.poll(timeout=0.05):
            try:
                val = quitChan.recv()
                if "done" in val:
                    break
                elif "quit" in val: #don't remove the quit command
                    quitChan.send("quit")
                else:
                    log.critical("Unexpected Value \"{}\" encountered while listening for a done alert from the mqtt client. Now Exiting.")
                    print("Unexpected Value \"{}\" encountered while listening for a done alert from the mqtt client. Now Exiting.")
                    sys.exit(ERROR_UNEXPECTED_ALERT)     
            except Exception as e:
                log.error("Error on quit Channel: {}".format(str(e)))

    #gracefully stop sensor, preventing new data being added to the dataQueue
    if not sensor.stop():
        log.warning("Sensor did not exit it's session properly. All the data currently in the queue will still be stashed but the Queue may be recieving new data.")
    
    #grab any data left in the data queue
    vals = None
    if not dataQueue.empty():
        vals = []
        while not dataQueue.empty():
            try:
                vals.append(dataQueue.get())
                vals.append("#End Payload\n") #seperates out each YAML inline file within the backup file
            except:
                log.warning("Unable to get single data value from queue.")
        
    
    #write to backup file
    if vals is not None:
        with open(backupFilePath, "a+", encoding="UTF-8") as f:
            for val in vals:
                f.write("{}\n".format(str(val)))

    #done    